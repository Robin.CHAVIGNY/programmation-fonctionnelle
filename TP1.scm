(define fact (lambda (n) (if (= 0  n) 1 (* n (fact (- n 1))))))
(fact 5)
(fact -5)
(fact 3.2)

(define some_int (lambda (n) (if (= 0 n) 0 (+ n (some_int (- n 1))))))
(some_int 4)

(define long (lambda (L) (if (null? L) 0 (+ 1 (long (cdr L))))))
(define k '(-1 2 3 4 5))
(cdddr k)
(long k)

(define renverse (lambda (L) (if (null?  L) () (append (renverse (cdr L)) (list (car L))))))
(renverse k)

(define carre (lambda (L) (if (null? L) () (append (list (* (car L) (car L))) (carre (cdr L))))))
(carre k)

(define nbpos (lambda (L) (if (null? L) 0 (if (< (car L) 0) (nbpos (cdr L)) (+ 1 (nbpos (cdr L)))))))
(nbpos k)

(define membre
  (lambda (x L)
    (if (null? L)
        #f
        (if (equal? x (car L))
            #t
            (membre x (cdr L))))))


(membre 7 k)

(define epure
  (lambda (L)
    (if (null? L)
        '()
        (if (membre (car L) (cdr L))
            (epure (cdr L))
            (append (list (car L)) (epure (cdr L)))))))



(define L3 '(1 2 4 4))

(epure L3)
(define L4 '(3 5 4))

(define unionL1L2
  (lambda (L1 L2)
    (epure (append L1 L2))))

(unionL1L2 L3 L4)

(define interL1L2
  (lambda (L1 L2)
    (if (null? L1)
        '()
        (if (membre (car L1) L2)
            (cons (car L1) (interL1L2 (cdr L1) L2))
            (interL1L2 (cdr L1) L2)))))

(interL1L2 L3 L4)

(define niv0
  (lambda (L)
    (if (null? L)
        '()
        (if (list? (car L))
            (append (niv0 (car L)) (niv0 (cdr L)))
            (cons (car L) (niv0 (cdr L)))))))

(define P '((1 5) 2 4 ((3) (4 8)) 3))

(niv0 P)

(define zip
  (lambda (L1 L2)
    (if (or (null? L1) (null? L2))
        '()
        (cons (list (car L1) (car L2)) (zip (cdr L1) (cdr L2))))))

(zip L3 L4)

(define prod
  (lambda (L1 L2)
    (if (or (null? L1) (null? L2))
        '()
        (append (list (list (car L1) (car L2))) (prod (list (car L1)) (cdr L2)) (prod (cdr L1) L2)))))

(prod L3 L4)

(define som_list
  (lambda (L)
    (if (null? L)
        0
        (if (list? (car L))
            (+ (som_list (car L)) (som_list (cdr L)))
            (if (integer? (car L))
                (+ (car L) (som_list (cdr L)))
                (som_list (cdr L)))))))

(som_list P)

(define triang1
  (lambda (n)
    (if (equal? 1  n)
        '(1)
        (niv0 (append (cons n (triang (- n 1))) (list n))))))

(define triang2
  (lambda (n)
    (if (equal? 1 n)
        '(n)
        (niv0 (append (cons
(triang1 8)


(define fibo
  (lambda (n)
    (if (= 1  n)
        (list 1 1)
        (cons (+ (car(fibo (- n 1))) (cadr(fibo(- n 1)))) (fibo (- n 1))))))

(define fibo2
  (lambda (n)
    (if (= 0 n)
        1
        (if (= 1 n)
            1
            (+ (fibo2(- n 1)) (fibo2(- n 2)))))))
(fibo2 30)

(define moy2
  (lambda (L)
    (if (null? L)
        (list 0 0)
        (let ((k (moy (cdr L))))
          (list (+ (/ (car L) (+ (cadr k) 1)) (* (car k) (/ (cadr k) (+ (cadr k) 1)))) (+ (cadr k) 1))))))
(moy2 L4)

(define ies
  (lambda (L n)
    (if (null? L)
        '(()()())
        (let ((g (ies (cdr L) n)))
                    (if (> (car L) n)
                        (list (cons (car L) (car g)) (cadr g) (caddr g))
                        (if (= (car L) n)
                            (list (car g) (cons (car L) (cadr g)) (caddr g))
                            (list (car g) (cadr g) (cons (car L) (caddr g)))))))))
(ies L4 2)

(define tri_ins
  (lambda (L)
    (if (null? L)
        ()
        (insere (car L) (tri_ins (cdr L))))))
(define insere
  (lambda  (x L)
    (if (null? L)
        (list x)
        (if (< x (car L))
            (cons x L)
            (cons (car L) (insere x (cdr L)))))))

(tri_ins L4)

(define (tri_sel L)
  (if (null? L)
      ()
      (
            
                    
(define (mapkar f L)
  (if (null? L)
      ()
      (cons (f (car L))(mapkar f (cdr L)))))
(mapkar sqrt '(16 81 9 121 8))

(define (carre_n x)
  (* x x))

(mapkar carre_n '(16 81 9 121 8))

(define (double x)
  (* 2 x))

(mapkar double '(16 81 9 121 8))
(map double '(16 81 9 121 8))

(define (mult x L)
  (if (null? L)
      ()
      (cons (* x (car L)) (mult x (cdr L)))))

(mult 3 '(16 81 9 121 8))

(define (pair x)
  (if (even? x)
      (list x)
      ()))

(map pair '(16 81 9 121 8))

(define (mapkan f L)
  (if (null? L)
      ()
      (append (f (car L)) (mapkan f (cdr L)))))

(mapkan pair '(16 81 9 121 8))

(define (sup9 x)
  (if (< x 9)
      ()
      (list x)))

(mapkan sup9 '(16 81 9 121 8))

(define (selection P L)
  (if (null? L)
      ()
      (if (P (car L))
          (cons (car L) (selection P (cdr L)))
          (selection P (cdr L)))))

(selection even? '(16 81 9 121 8))

(define (comp f g)
  (lambda (x)
    (f (g x))))

(define (demi x)
  (* 0.5 x))

(map (comp sup9 demi) '(16 81 9 121 8))

(define (derrive f)
  (lambda (x)
    (/ (- (f(+ x 0.0001)) (f(- x 0.0001))) (* 2 0.0001))))

(map (derrive log) '(16 81 9 121 8))

((derrive log) 2)

(define r '(6 5 3 1 8 7 2 4))

(define (lili L)
  (if (null? L)
      ()
      (cons (list (car L)) (lili (cdr L)))))

(define (lili+ L)
  (if (null?  L)
      ()
      ((define t (long r))
       
(lili+ r)

(long r)

(define (tri_fus K)
  ((define L (lili K))
   
  (if (null? L)
      ()
      ((define L1 (list (car L)))
       (define L2 (list (cadr L)))
       (if (< (car L1) (car L2))
          (cons (car L1) (cadr L) (tri_fus (cddr L)))
          (cons (cadr L) (car L) (tri_fus (cddr L)))))))

(tri_fus r)
(<  5) 


(define E '(0 1 2))
(define (PC E n)
  (if (equal? 0 n)
      '(())
      (let ((prec (PC E (- n 1))))
        (append-map (lambda (x)
                      (map (lambda (y) (cons x y)) prec))
                    E))))

(PC E 3)

(define (tra M)
  (if (null? M)
      0
      (+ (caar M) (tra (map cdr (cdr M))))))

(tra '((1 1 2 3)
       (2 3 5 6)
       (3 5 0 3)
       (2 3 3 4)))

(define (transp M)
  (if (null? (car M))
      ()
      (cons (map car M) (transp (map cdr M)))))

(transp '((1 2 3)
          (4 5 6)
          (7 8 9)
          (10 11 12)))
(apply + (map * '(1 2) '(3 4)))

(define (produidui M V)
  (if (null? M)
      ()
      (if (null? V)
          ()
          (apply + (map * ( V))))))


(define (tous-egaux L)
  (if (null? (cdr L))
      #t
      (if (equal? (car L) (cadr L))
          (tous-egaux (cdr L))
          ()
          )))

(define tous-egauxU (lambda (L P) 
                      (if (null? (cdr L))
                          #t
                          (if (P (car L) (cadr L))
                               (tous-egauxU (cdr L) P)
                               ()))))
(define (qqs L P)
  (if (null? L)
      #t
      (if (P (car L))
          (qqs (cdr L) P)
          #f)))
(define (tous-egauxCU L)
  (qqs (cdr L)
       (lambda(x)
         (equal? x (car L)))))

(define (existe L P)
  (if (null? L)
      #f
      (if (P (car L))
             #t
             (existe (cdr L) P))))
(define (tous-egauxCE L)
  (if (null? L)
      #t
      (not (existe (cdr L)
                   (lambda (x)
                     (not (equal? x (car L))))))))



(tous-egauxCE '(1 1 2 1))





(define (recN n n0 uinit f)
  (if (= n n0)
      uinit
      (f (recN (- n 1) n0 uinit f) n)))
(define (fac n)
  (recN n 0 1 *))

(fac 5)
(recN 10 0 0 +)

(define (fibo n)
  (car (fiborec n)))
(define (fiborec n)
  (recN n 0 '(1 0) (lambda (un-1 n)
                     (list (+ (car un-1)
                              (cadr un-1))
                           (car un-1)))))


(define SR (lambda (L B C)
             (if (null? L)
                 B
                 (C (car L)
                    (SR (cdr L) B C)))))

(define (someliste L)
  (SR L 0 (lambda (tete RR)
            (+ tete RR))))
(define (prodliste L)
  (if (null? L)
      0
      (SR L 1 *)))

(define (long L)
  (SR L 0 (lambda (tete RR)
            (+ 1 RR))))

(define (renverse L)
  (SR L () (lambda (e RR) (append RR (list e)))))

(define (miroir L)
  (SR L () (lambda (e RR) (
  
              
